# -*- coding: utf-8 -*-
import os
import ConfigParser

BASE_DIR = os.path.dirname(os.path.dirname(__file__))

my_config = ConfigParser.ConfigParser()
my_config.readfp(open(BASE_DIR + '/app/settings.ini'))

SECRET_KEY = my_config.get('main', 'secret_key')
DEBUG = my_config.getboolean('main', 'debug')
TEMPLATE_DEBUG = my_config.getboolean('main', 'template_debug')
ADMINS = ((my_config.get('main', 'admin_name'), my_config.get('main', 'admin_email')),)
ALLOWED_HOSTS = [my_config.get('main', 'allowed_hosts')]
ROOT_URLCONF = 'app.urls'
WSGI_APPLICATION = 'app.wsgi.application'
LANGUAGE_CODE = my_config.get('main', 'language')
TIME_ZONE = my_config.get('main', 'timezone')
USE_I18N = my_config.getboolean('main', 'use_i18n')
USE_L10N = my_config.getboolean('main', 'use_l10n')
USE_TZ = my_config.getboolean('main', 'use_tz')

INSTALLED_APPS = (
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'app',
)
MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': my_config.get('mysql', 'name'),
        'USER': my_config.get('mysql', 'user'),
        'PASSWORD': my_config.get('mysql', 'password'),
        'HOST': my_config.get('mysql', 'host'),
    }
}
TEMPLATE_DIRS = (BASE_DIR + '/app/templates/',)

STATIC_ROOT = BASE_DIR + '/static/'
MEDIA_URL = '/media/'
MEDIA_ROOT = BASE_DIR + '/media/'
STATICFILES_DIRS = ()
STATIC_URL = '/static/'
STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
)

EMAIL_HOST = my_config.get('email', 'smtp')
EMAIL_HOST_PASSWORD = my_config.get('email', 'password')
EMAIL_HOST_USER = my_config.get('email', 'user')
EMAIL_PORT = my_config.get('email', 'port')
EMAIL_USE_TLS = my_config.get('email', 'use_tls')
EMAIL_USE_SSL = my_config.get('email', 'use_ssl')

ERROR_LOG_FILE = BASE_DIR + '/logs/' + 'error.log'
INFO_LOG_FILE = BASE_DIR + '/logs/' + 'info.log'

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'filters': {
        'require_debug_false': {
            '()': 'django.utils.log.RequireDebugFalse'
        },
        'require_debug_true': {
            '()': 'django.utils.log.RequireDebugTrue'
        }
    },
    'formatters': {
        'main_formatter': {
            'format': '%(levelname)s:%(name)s: %(message)s '
                      '(%(asctime)s; %(filename)s:%(lineno)d)',
            'datefmt': "%Y-%m-%d %H:%M:%S",
        },
    },
    'handlers': {
        'mail_admins': {
            'level': 'ERROR',
            'filters': ['require_debug_false'],
            'class': 'django.utils.log.AdminEmailHandler'
        },
        'console': {
            'level': 'DEBUG',
            'filters': ['require_debug_true'],
            'class': 'logging.StreamHandler',
            'formatter': 'main_formatter',
        },
        'production_file': {
            'level': 'INFO',
            'class': 'logging.handlers.RotatingFileHandler',
            'filename': INFO_LOG_FILE,
            'maxBytes': 1024 * 1024 * 5,
            'backupCount': 7,
            'formatter': 'main_formatter',
            'filters': ['require_debug_false'],
        },
        'debug_file': {
            'level': 'DEBUG',
            'class': 'logging.handlers.RotatingFileHandler',
            'filename': ERROR_LOG_FILE,
            'maxBytes': 1024 * 1024 * 5,
            'backupCount': 7,
            'formatter': 'main_formatter',
            'filters': ['require_debug_true'],
        },
        'null': {
            "class": 'django.utils.log.NullHandler',
        }
    },
    'loggers': {
        'django.request': {
            'handlers': ['mail_admins', 'console'],
            'level': 'ERROR',
            'propagate': True,
        },
        'django': {
            'handlers': ['null', ],
        },
        'py.warnings': {
            'handlers': ['null', ],
        },
        '': {
            'handlers': ['console', 'production_file', 'debug_file'],
            'level': "DEBUG",
        },
    }
}