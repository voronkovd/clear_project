# -*- coding: utf-8 -*
from uuid import uuid4
from django.conf import settings
from django.db import models
import os


def path_and_rename(path):
    def wrapper(instance, filename):
        ext = filename.split('.')[-1]
        if instance.pk:
            filename = '{}.{}'.format(instance.pk, ext)
        else:
            filename = '{}.{}'.format(uuid4().hex, ext)
        return os.path.join(path, filename)

    return wrapper


"""
class YourModel(models.Model):


    class Meta:
        db_table = 'your_table_name'
        verbose_name = u''
        verbose_name_plural = u''

    def __unicode__(self):
        return u''

"""